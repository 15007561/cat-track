#pragma once
#include <cinttypes>
#include "Randomizer.h"
#include "Tracker.h"

class HeartRateSensor
{
private:
	enum DebugKey
	{
		None= 0,
		HR_Zero = 48,		// '0' Set heart rate to 0
		HR_Low = 108,		// 'l' Set heart rate below minimum
		HR_Normal = 110,	// 'n' Set heart rate to normal
		HR_High = 104,		// 'h' Set heart rate above maximum
	};

public:
	// Description: Get a random heart rate.
	// Parameter(s): Previous heart rate.
	// Returns: uint16_t based on the previous heart rate.
	int16_t getHeartRate(int16_t);

	// Description: Get heart rate based on debug key.
	// Parameter(s): Previous heart rate, debug key.
	// Returns: uint16_t based on the debug key chosen.
	int16_t getHeartRate(int16_t, int);
};