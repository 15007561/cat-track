#pragma once
#include <random>
#include <cinttypes>

class Randomizer
{
private:
	std::random_device m_seed;

public:
	// Description: Get a random double within the given range.
	// Parameter(s): Minimum, maximum as doubles.
	// Returns: double
	double getDouble(double, double);
	
	// Description: Get a random integer of type passed within the given range.
	// Parameter(s): Minimum, maximum as integer of type given.
	// Returns: Integer of type given. (int, uint16_t, etc.)
	template <class T>
	T getInteger(T min, T max)
	{
		std::default_random_engine dre(m_seed());
		std::uniform_int_distribution<T> uid(min, max);	
		
		return uid(dre);
	}
};
