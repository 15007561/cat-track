#include "Coordinates.h"

// GPS coordinates minimum and maxiumum using decimal degrees.
// Use six decimal places for 0.11 meter accuracy
const double Coordinates::LATITUDE_MIN = -90.0;
const double Coordinates::LATITUDE_MAX = 90.0;
const double Coordinates::LONGITUDE_MIN = -180.0;
const double Coordinates::LONGITUDE_MAX = 180.0;
const uint16_t Coordinates::PRECISION = 6;

double Coordinates::getLatitude() const
{
	return m_latitude;
}

bool Coordinates::setLatitude(double latitude)
{
	// Check if the latitude is within range
	if (latitude < LATITUDE_MIN || latitude > LATITUDE_MAX)
		return false;
	else
	{
		m_latitude = latitude;
		return true;
	}
}

double Coordinates::getLongitude() const
{
	return m_longitude;
}

bool Coordinates::setLongitude(double longitude)
{
	// Check if the longitude is within range
	if (longitude < LONGITUDE_MIN || longitude > LONGITUDE_MAX)
		return false;
	else
	{
		m_longitude = longitude;
		return true;
	}
}