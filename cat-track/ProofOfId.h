#pragma once
#include <string>

class ProofOfId
{
private:
	std::string m_proofId;

public:
	// Description: constructor
	// Parameter(s): none
	ProofOfId() : m_proofId("") {} // initialised string
	
	// Destructor
	~ProofOfId() { m_proofId = ""; } // reset before destruction

	// Description: Get the proof of ID.
	// Parameter(s): none
	// Returns: string
	std::string getProofId() { return m_proofId; }

	// Description: Set the proof of ID.
	// Parameter(s): string
	// Returns: nonthing
	void setProofId(std::string newProofId) { m_proofId = newProofId; }
};
