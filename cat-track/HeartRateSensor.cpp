#include "HeartRateSensor.h"

int16_t HeartRateSensor::getHeartRate(int16_t previous)
{
	const int16_t VARIATION = 3;

	int16_t minValue = 0;
	int16_t maxValue = 0;
	Randomizer rnd;

	// If no previous heart rate, randomize new heart rate
	if (previous == -1)
	{
		minValue = Tracker::HEART_RATE_MIN;
		maxValue = Tracker::HEART_RATE_MAX;
	}
	// Change the heart rate based on previous rate using variation, except on 0.
	else if (previous != 0)
	{
		// Prevent integer wrapping
		if (previous < VARIATION)
			minValue = Tracker::HEART_RATE_MIN;
		else
			minValue = previous - VARIATION;

		// Prevent integer wrapping
		if (INT16_MAX - previous < VARIATION)
			maxValue = Tracker::HEART_RATE_MAX;
		else
			maxValue = previous + VARIATION;
	}
	// previous is 0, so leave it at 0.
	else
		return 0;

	return rnd.getInteger<int16_t>(minValue, maxValue);
}

int16_t HeartRateSensor::getHeartRate(int16_t previous, int key)
{
	const int16_t VARIATION = 10;

	// Check debug key
	switch (key)
	{
	case DebugKey::HR_Zero:		// Set heart rate to zero
		return 0;
		break;
	case DebugKey::HR_Low:		// Set heart rate below minimum
		return Tracker::HEART_RATE_MIN - VARIATION;
		break;
	case DebugKey::HR_Normal:	// Set heart rate to value within proper range
		return getHeartRate(-1);
		break;
	case DebugKey::HR_High:		// Set heart rate above the maximum
		return Tracker::HEART_RATE_MAX + VARIATION;
		break;
	case DebugKey::None:		// Set heart rate to value base on previous
		return getHeartRate(previous);
		break;
	default:
		return getHeartRate(previous);
		break;
	}
}