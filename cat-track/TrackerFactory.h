#pragma once
#include <memory>
#include "Role.h"
#include "Tracker.h"

class TrackerFactory
{
public:
	std::shared_ptr<Tracker> accessResource(Role& role)
	{
		return std::shared_ptr<Tracker>(Tracker::newTracker(role.getRoleName()));
	}
};
