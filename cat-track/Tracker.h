#pragma once
#include <cinttypes>
#include <iostream>
#include <memory>

#include "Coordinates.h"
#include "GpsSensor.h"
#include "Permission.h"

class Tracker
{
private:	
	// Using cinttypes since calulations are used on these values
	int16_t m_heartRate; // one value read from sensor
	uint16_t m_boundaryRadius;
	uint16_t m_batteryLevel;
	bool m_alarmState;
	Tracker();

public:
	static const int16_t HEART_RATE_MIN;
	static const int16_t HEART_RATE_MAX;
	static const uint16_t BOUNDARY_MIN;
	static const uint16_t BOUNDARY_MAX;
	// Using smart pointers
	std::unique_ptr<Coordinates> homeCoords;
	std::unique_ptr<Coordinates> currentCoords; // two values read by sensor, latitude and longitude

	~Tracker();

	// Description: Get the current heart rate.
	// Parameters: None
	// Returns: unsigned 16-bit integer
	int16_t getHeartRate() const { return m_heartRate; }

	// Description: Set heart rate.
	// Parameter: unsigned 16-bit integer
	// Returns: nothing
	void setHeartRate(int16_t);

	// Description: Get the boundary radius, which is the max
	//              distance from the home coordinates.
	// Parameters: None
	// Returns: unsigned 16-bit integer
	uint16_t getBoundaryRadius() const { return m_boundaryRadius; }

	// Description: Set the boundary radius.
	// Parameter: unsigned 16-bit integer
	// Returns: bool. True on success, false on failure.
	bool setBoundaryRadius(uint16_t);

	// Description: Set the alarm on or off.
	// Parameter(s): Boolean value to set the alarm.
	// Returns: nothing
	void setAlarm(bool);

	// Description: Gets the state of the alarm.
	// Parameter(s): none
	// Returns: bool, true if on, false if off.
	bool getAlarm();

	// Description: Sets the battery level.
	// Parameter(s): unsigned 16-bit integer in the range of 0-100.
	// Returns: nothing
	void setBatteryLevel(uint16_t);

	// Description: Get's the battery level.
	// Parameter(s): none
	// Returns: unsigned 16-bit integer
	uint16_t getBatteryLevel();

	// Description: Create a new tracker if the role has the access.
	// Parameter(s): Role as unsigned integer
	// Returns: shared_ptr<Tracker>
	static std::shared_ptr<Tracker> newTracker(unsigned);
};
