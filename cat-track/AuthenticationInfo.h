#pragma once
#include <map>
#include <string>

class AuthenticationInfo
{
private:
	std::map<std::string, std::string> m_users;

public:
	// Description: Constructor-adds users information.
	// Parameter(s): none
	AuthenticationInfo();

	// Destructor
	~AuthenticationInfo();

	// Description: Validate the user's credentials.
	// Parameter(s): string as username, string as password
	// Returns: bool. True if valid, false if invalid.
	bool validateUser(std::string, std::string);
};