#pragma once
#define _USE_MATH_DEFINES // for Pi
#include <cmath>
#include <iomanip>
#include <memory>
#include <sstream>
#include <string>
#include "Tracker.h"

class Actuator
{
private:
	std::shared_ptr<Tracker> m_tracker;
	bool m_useHeartRateSensor;

public:
	// Description: Contstructor
	// Parameter(s): shared_ptr<Tracker>	
	Actuator(std::shared_ptr<Tracker> tracker) : m_tracker(tracker), m_useHeartRateSensor(true) { }

	// Description: Checks the state of the heart rate.
	//				Toggles the	heart rate sensor if none found.
	// Parameter(s): 
	// Returns: string
	std::string checkHeartRate();

	// Description: Checks if the tracker is within the boundary radius.
	// Parameter(s): none
	// Returns: string
	std::string checkBoundaryStatus();

	// Description: Checks if the battery level is lower than 10%.
	// Parameter(s): 
	// Returns: bool
	bool hasLowBatteryLevel();

private:
	// Description: Checks if the tracker is within the boundary radius.
	// Parameter(s): none
	// Returns: bool. True if in bounds, false if out of bounds.
	bool isInBounds();
};
