#include "Permission.h"

Permission::Permission()
{
	m_roleResourceAccessMap[Administrator] = View | Modify | Manage;
	m_roleResourceAccessMap[User] = View | Modify;
	m_roleResourceAccessMap[Viewer] = View;
}

bool Permission::checkPermission(unsigned role, unsigned accessType)
{
	if (m_roleResourceAccessMap[role] & accessType)
		return true;
	else
		return false;
}