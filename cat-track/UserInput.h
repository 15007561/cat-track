#pragma once
#include <cinttypes>
#include <iostream>
#include <sstream>
#include <string>
#include <regex>

class UserInput
{
private:
	// Description: Check if a string of numbers exceeds the largest value possible.
	//				Assumes it was already checked against a regex pattern.
	// Parameter(s): string
	// Returns: bool. true if size is too large, false if within range.
	// Example: uint8_t = 0-255. If the user enters "300", then the conversion
	//			to uint8_t will yield a value of 0. Since we know the input has more than one character, 
	//			then if we get 0 as a value, we know the input was too large.
	template <class T>
	static bool correctSize(const std::string& input)
	{
		std::istringstream iss;
		T value = 0;

		// Convert
		iss.str(input);
		iss >> value;

		// If input has more than 1 character and value is zero, then bad size!
		if (input.size() > 1 && value == 0)
			return false;
		else
			return true;
	}

	// Description: Get a number based on the type given, checked against a regular expression.
	// Parameter(s): regex-Regular expression of allowed input.
	//				 string-Message to the user explaining the expected input.
	// Returns: Type given
	template <class T>
	static T getNumber(std::regex regExp, std::string formatMsg)
	{
		std::istringstream iss;
		std::string input = ""; // initialised string
		T value = 0;
		bool goodInput = false;

		std::getline(std::cin, input); // get unformatted input
		while (goodInput == false)
		{
			// Check input against regular expression (checks format)
			if (std::regex_match(input, regExp) == false)
				goodInput = false;
			// check if correct size
			else if (correctSize<T>(input) == false)
				goodInput = false;
			else
				goodInput = true;

			// If bad input, prompt for new input
			if (goodInput == false)
			{
				std::cout << formatMsg;
				std::getline(std::cin, input); // get unformatted input
			}
		}

		// convert
		iss.str(input);
		iss >> value;

		return value;
	}

	// Description: Get a string using the regular expression.
	// Parameter(s): regex-Regular expression of allowed input.
	//				 string-Message to the user explaining the expected input.
	// Returns: string
	static std::string getString(std::regex, std::string);

public:
	// Description: Get an unsigned 16-bit integer.
	// Parameter(s): none
	// Returns: uint16_t
	static uint16_t getUInt16(std::string prompt = "Bad input! Please enter a positive 16-bit integer: ");

	// Description: Get a signed 16-bit integer.
	// Parameter(s): none
	// Returns: int16_t
	static int16_t getInt16(std::string prompt = "Bad input! Please enter a 16-bit integer: ");

	// Description: Get a double.
	// Parameter(s): none
	// Returns: double
	static double getDouble(std::string prompt = "Bad input! Please enter a floating point number: ");

	// Description: Prompts the user for a password.
	// Parameter(s): none
	// Returns: string
	static std::string getPassword();

	// Description: Prompts the user for a username.
	// Parameter(s): none
	// Returns: string
	static std::string getUsername();
};