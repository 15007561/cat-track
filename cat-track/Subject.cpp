#include "Subject.h" 

Subject::Subject() : m_proofId("")
{
	std::cout << "\nEnter Username: ";
	m_username = UserInput::getUsername();

	std::cout << "\nEnter password: ";
	m_password = UserInput::getPassword();

	// Set the user's role
	m_role.setRoleName(m_username);
}

Subject::~Subject()
{
	m_username = "";
	m_password = "";
	m_proofId = "";
}

void Subject::setProofId(std::string newProofId)
{
	m_proofId = newProofId;
}

std::shared_ptr<Tracker> Subject::getTracker()
{
	std::unique_ptr<TrackerFactory> trackerFactory(new TrackerFactory());

	m_tracker = trackerFactory->accessResource(m_role);
	return m_tracker;
}