#include "UserInput.h"

// Using regex to check unfiltered input

uint16_t UserInput::getUInt16(std::string prompt)
{
	const std::regex UNSIGNED_DIGIT("[[:digit:]]{1,5}");
	return getNumber<uint16_t>(UNSIGNED_DIGIT, prompt);
}

int16_t UserInput::getInt16(std::string prompt)
{
	const std::regex SIGNED_DIGIT("-?[[:digit:]]{1,5}");
	return getNumber<int16_t>(SIGNED_DIGIT, prompt);
}

double UserInput::getDouble(std::string prompt)
{
	const std::regex DOUBLE_DIGIT("-?[[:digit:]]*\\.?[[:digit:]]+");
	return getNumber<double>(DOUBLE_DIGIT, prompt);
}

std::string UserInput::getString(std::regex regExp, std::string formatMsg)
{
	std::string input = "";
	bool goodInput = false;

	std::getline(std::cin, input); // get unformatted input
	while (goodInput == false)
	{
		// Check if the string matches the regular expression
		if (std::regex_match(input, regExp) == false)
			goodInput = false;
		else
			goodInput = true;

		// If bad input, ask for new input
		if (goodInput == false)
		{
			std::cout << formatMsg;
			std::getline(std::cin, input); // get unformatted input
		}
	}

	return input;
}

std::string UserInput::getPassword()
{
	const std::regex PASSWORD("[a-zA-Z0-9]{1,30}");
	return getString(PASSWORD, "Please enter only alphanumeric characters: ");
}

std::string UserInput::getUsername()
{
	const std::regex USERNAME("[a-z0-9]{1,30}");
	return getString(USERNAME, "Please enter only lowercase alphanumeric characters: ");
}