#include "Tracker.h"

const int16_t Tracker::HEART_RATE_MIN = 120;
const int16_t Tracker::HEART_RATE_MAX = 220;
const uint16_t Tracker::BOUNDARY_MIN = 10;
const uint16_t Tracker::BOUNDARY_MAX = 1000;

Tracker::Tracker() : m_heartRate(-1), m_boundaryRadius(50),
	m_batteryLevel(100), m_alarmState(false)
{
	// Set home and current coordinates to the same random gps coordinates
	std::unique_ptr<GpsSensor> gps(new GpsSensor());
	homeCoords = gps->getCurrentCoords();
	currentCoords = std::unique_ptr<Coordinates>(new Coordinates());
	currentCoords->setLatitude(homeCoords->getLatitude());
	currentCoords->setLongitude(homeCoords->getLongitude());
}

Tracker::~Tracker()
{
	// Resetting variables to default before destruction
	m_heartRate = -1;
	m_boundaryRadius = 0;
	m_alarmState = false;
}

void Tracker::setHeartRate(int16_t heartRate)
{
	m_heartRate = heartRate;
}

bool Tracker::setBoundaryRadius(uint16_t radius)
{
	// Check if the raidus is within the min/max range
	if (radius >= BOUNDARY_MIN && radius <= BOUNDARY_MAX)
	{
		m_boundaryRadius = radius;
		return true;
	}
	else
		return false;
}

void Tracker::setAlarm(bool newState)
{
	m_alarmState = newState;
}

bool Tracker::getAlarm()
{
	return m_alarmState;
}

void Tracker::setBatteryLevel(uint16_t level)
{
	// Check if the level is within range
	if (level >= 0 && level <= 100)
		m_batteryLevel = level;
}

uint16_t Tracker::getBatteryLevel()
{
	return m_batteryLevel;
}

std::shared_ptr<Tracker> Tracker::newTracker(unsigned role)
{
	const unsigned ACCESS_TYPE = AccessTypes::View; // Minimum access type

	std::unique_ptr<Permission> permission(new Permission);

	// If the role has the access type then create and return the new tracker
	if (permission->checkPermission(role, ACCESS_TYPE))
	{
		return std::shared_ptr<Tracker>(new Tracker());
	}
	else
		return nullptr;
}