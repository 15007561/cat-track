#include "Logger.h"
#include <iostream>
#include <fstream>

const std::string Logger::DEFAULT_LOGFILE = "log.txt";

// Initialised string
Logger::Logger() : m_logFile(DEFAULT_LOGFILE) {}

Logger::Logger(std::string logFile)
{
	if (logFile != "")
		m_logFile = logFile;
	else
	{
		m_logFile = DEFAULT_LOGFILE;
		std::cout << "\nLog filename cannot be empty! Using default log file: " << DEFAULT_LOGFILE << "\n";
	}
}

// Cleared variable before relase
Logger::~Logger()
{
	m_logFile = "";
}

void Logger::log(std::string msg)
{
	// Using try/catch for file error exception catching
	try
	{
		std::ofstream logFile(m_logFile, std::ios_base::app);
		// Set exception bits
		logFile.exceptions(std::ofstream::failbit | std::ofstream::badbit | std::ofstream::eofbit);
		logFile << msg << "\n";
		logFile.close();
	}
	catch (std::exception e)
	{
		std::cout << "\nError writing log!: " << e.what() << std::endl;
	}
}