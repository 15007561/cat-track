#pragma once
#include <chrono>
#include <cinttypes>
#include <conio.h>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>

#include "Actuator.h"
#include "Authenticator.h"
#include "GpsSensor.h"
#include "HeartRateSensor.h"
#include "Logger.h"
#include "ProofOfId.h"
#include "Subject.h"
#include "Tracker.h"
#include "UserInput.h"

namespace Choices
{
	enum Choice : uint16_t
	{
		ViewTracker = 1,
		SetHome,
		SetBounds,
		Logout,
		Exit
	};
}

class Menus
{
private:
	// Using smart pointers
	std::shared_ptr<Tracker> m_tracker;
	std::shared_ptr<Subject> m_subject;
	std::unique_ptr<ProofOfId> m_proofId;

public:
	Menus() : m_subject(nullptr), m_proofId(new ProofOfId()) { }

	// Description: Show the main menu.
	// Parameter(s): none
	// Returns: nothing
	void show();

private:
	// Description: The main menu for the user.
	// Parameter(s): none
	// Returns: uint16_t representing menu choice
	uint16_t mainMenu();

	// Description: View simulated tracker data.
	// Parameter(s): none
	// Returns: nothing
	void viewTracker();

	// Description: Prompt the user for their home GPS coordinates.
	// Parameter(s): none
	// Returns: nothing
	void setHomeCoords();

	// Description: Prompt the user for a boundary radius.
	// Parameter(s): none
	// Returns: nothing
	void setBoundaryRadius();
};