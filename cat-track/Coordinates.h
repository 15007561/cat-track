#pragma once
#include <cinttypes>

class Coordinates
{
private:
	// In decimal degrees
	double m_latitude;
	double m_longitude;

public:
	static const double LATITUDE_MIN;
	static const double LATITUDE_MAX;
	static const double LONGITUDE_MIN;
	static const double LONGITUDE_MAX;
	static const uint16_t PRECISION;

	// Default constructor
	Coordinates() : m_latitude(0), m_longitude(0) {}

	// Destructor
	~Coordinates()
	{
		// Resetting data to default before destruction
		m_latitude = 0;
		m_longitude = 0;
	}
	
	// Description: Get the latitude.
	// Parameter(s): none
	// Returns: double
	double getLatitude() const;

	// Description: Set the latitude using decimal degrees.
	// Parameter: Double
	// Returns: bool. True on success, false on failure.
	bool setLatitude(double);
	
	// Description: Get the longitude.
	// Parameter(s): none
	// Returns: double
	double getLongitude() const;

	// Description: Set the longitude using decimal degrees.
	// Parameter: double
	// Returns: bool. True on success, false on failure.
	bool setLongitude(double);
};
