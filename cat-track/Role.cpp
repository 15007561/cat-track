#include "Role.h"

Role::Role() : m_roleName(0)
{
	m_userRoleAccessMap["john"] = Administrator;
	m_userRoleAccessMap["sam"] = Viewer;
	m_userRoleAccessMap["fran"] = User;
}

void Role::setRoleName(std::string subjectName)
{
	m_roleName = m_userRoleAccessMap[subjectName];
}