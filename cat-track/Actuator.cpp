#include "Actuator.h"

std::string Actuator::checkHeartRate()
{	
	const std::string NO_HEART_RATE = "No heart rate!\nThe tracker is not attached or it's time for a new cat.\nTurning off sensor display.";
	const std::string ABNORMAL_HEART_RATE = "Your cat's heart rate is too ";
	const std::string HEART_RATE = "Heart rate: ";

	int16_t heartRate = m_tracker->getHeartRate();
	std::ostringstream oss;
	std::string stateMessage = "";

	// Convert heart rate to string
	oss << m_tracker->getHeartRate();

	// Check if the heart rate is too low
	if (heartRate < m_tracker->HEART_RATE_MIN)
		if (heartRate == 0) // No heart rate
		{
			if (m_useHeartRateSensor) // if sensor is in use, stop using it
			{
				m_useHeartRateSensor = false;
				stateMessage = NO_HEART_RATE;
			}
			else
				stateMessage = "";
		}
		else // low heart rate
		{
			m_useHeartRateSensor = true;
			stateMessage = ABNORMAL_HEART_RATE + "low!\n" + HEART_RATE + oss.str();
		}
	// Check if the heart rate is too high
	else if (heartRate > m_tracker->HEART_RATE_MAX)
	{
		m_useHeartRateSensor = true;
		stateMessage = ABNORMAL_HEART_RATE + "high!\n" + HEART_RATE + oss.str();		
	}
	else // Normal heart rate
	{
		m_useHeartRateSensor = true;
		stateMessage = HEART_RATE + oss.str();
	}

	return stateMessage;
}

std::string Actuator::checkBoundaryStatus()
{
	std::string stateMessage = "";

	// Check if the cat is within the boundary radius
	if (isInBounds() == false)
	{
		m_tracker->setAlarm(true);
		stateMessage = "Your cat is out of bounds and is getting away!\nAlarm is on!";
	}
	else // within bounds, turn alarm off
	{
		m_tracker->setAlarm(false);
		stateMessage = "";
	}

	return stateMessage;
}

bool Actuator::isInBounds()
{
	// Veness, C. (2016) Calculate distance, bearing and more between Latitude/Longitude points.
	// Available at: http://www.movable-type.co.uk/scripts/latlong.html
	// (Accessed: 2 Jan 2017)
	const double EARTH_RADIUS = 6371e3;
	double currLat = m_tracker->currentCoords->getLatitude();
	double currLon = m_tracker->currentCoords->getLongitude();
	double homeLat = m_tracker->homeCoords->getLatitude();
	double homeLon = m_tracker->homeCoords->getLongitude();

	double latitudeDiff = currLat - homeLat;
	double longitudeDiff = currLon - homeLon;
	double currLatDegree = (currLat * M_PI) / 180;
	double homeLatDegree = (homeLat * M_PI) / 180;
	double a = 0.0;
	double c = 0.0;
	double distance = 0.0;

	// Convert to radians
	latitudeDiff = (latitudeDiff * M_PI) / 180;
	longitudeDiff = (longitudeDiff * M_PI) / 180;

	// Calculate distance
	a = sin(latitudeDiff / 2)  * (latitudeDiff / 2) +
		cos(homeLatDegree) * cos(currLatDegree) *
		sin(longitudeDiff / 2) * (longitudeDiff / 2);
	c = 2 * atan2(sqrt(a), sqrt(1 - a));
	distance = EARTH_RADIUS * c;

	if (distance > static_cast<double>(m_tracker->getBoundaryRadius()))
		return false;
	else
		return true;
}

bool Actuator::hasLowBatteryLevel()
{
	const uint16_t LOW_LEVEL = 10;

	if (m_tracker->getBatteryLevel() < LOW_LEVEL)
		return true;
	else
		return false;
}