#include "Menus.h"

using namespace std::chrono_literals;

void Menus::show()
{
	uint16_t menuChoice = 0;
	Authenticator auth = Authenticator();
	GpsSensor gpsSensor = GpsSensor();

	while (menuChoice != Choices::Exit)
	{
		// Have new subject login and authenticate credentials
		// Using authentication pattern
		m_subject.reset(new Subject());
		m_proofId = auth.authenticateUser(m_subject);

		// If valid subject, show main menu
		if (m_subject->getProofId() == m_proofId->getProofId() && m_proofId->getProofId() != "")
		{
			// Get the tracker via a valid user
			// Using role based access control pattern
			m_tracker = m_subject->getTracker();
			
			// If the user has access, show the main menu
			if (m_tracker != nullptr)
				menuChoice = mainMenu();
		}

		// If the user logs out, then destroy the tracker
		if (m_tracker != nullptr)
			m_tracker.reset();
	} // while not exiting
}

uint16_t Menus::mainMenu()
{
	uint16_t menuChoice = 0;

	while (menuChoice != Choices::Exit && menuChoice != Choices::Logout)
	{
		std::cout << "\n1. View Cat Tracker";
		std::cout << "\n2. Set Home Coordinates: ";
		std::cout << std::fixed << std::setprecision(Coordinates::PRECISION);
		std::cout << m_tracker->homeCoords->getLatitude() << ", " << m_tracker->homeCoords->getLongitude();
		std::cout << "\n3. Set Boundary Radius: " << m_tracker->getBoundaryRadius();
		std::cout << "\n4. Logout";
		std::cout << "\n5. Exit";
		std::cout << "\n\nEnter your choice: ";

		menuChoice = UserInput::getUInt16();

		switch (menuChoice)
		{
		case Choices::ViewTracker:	// View tracker data
			viewTracker();
			break;
		case Choices::SetHome:		// Set home coordinates
			setHomeCoords();
			break;
		case Choices::SetBounds:	// Set restriction boundary
			setBoundaryRadius();
			break;
		case Choices::Logout:		// Log out current user
			std::cout << "\nLogging out...\n";
			break;
		case Choices::Exit:			// Close the program
			std::cout << "\nGood bye!\n";
			break;
		default:					// Invalid selection
			std::cout << "\nInvalid choice!\n";
			break;
		}
	} // while not exit/logout

	return menuChoice;
}

void Menus::viewTracker()
{
	const int KEY_STOP = 115; // s

	Actuator trackerActuator = Actuator(m_tracker);
	GpsSensor gpsSensor = GpsSensor();
	HeartRateSensor hrSensor = HeartRateSensor();
	Logger dataLogger;
	std::ostringstream oStrStream;
	std::string logMessage = ""; // inistialised string and other variables
	uint16_t timePassed = 0;
	bool stopView = false;
	int key = 0;
	int ignoreKey = 0;

	std::cout << "\n\nPress any key to start.\nPress 's' to stop.\n";
	ignoreKey = _getch();

	// Set precision
	oStrStream << std::fixed << std::setprecision(Coordinates::PRECISION);
	std::cout << std::fixed << std::setprecision(Coordinates::PRECISION);

	// Set current coordinates to home
	m_tracker->currentCoords->setLatitude(m_tracker->homeCoords->getLatitude());
	m_tracker->currentCoords->setLongitude(m_tracker->homeCoords->getLongitude());

	// Start viewing the tracker
	while (stopView == false)
	{
		// Sleep thread for 1 second
		std::this_thread::sleep_for(1s);
		timePassed++;

#if !_DEBUG // Don't allow debug keys if not in debug mode
		// Get simulated data
		m_tracker->setHeartRate(hrSensor.getHeartRate(m_tracker->getHeartRate()));
		m_tracker->currentCoords = gpsSensor.getCoordinates(m_tracker);
#else
		// Get simulated data - debug keys enabled
		m_tracker->setHeartRate(hrSensor.getHeartRate(m_tracker->getHeartRate(), key));
		m_tracker->currentCoords = gpsSensor.getCoordinates(m_tracker, key);
		key = 0;
#endif

		// Check heart rate
		logMessage = trackerActuator.checkHeartRate();
		if (logMessage != "")
		{
			std::cout << logMessage << std::endl;
			dataLogger.log(logMessage);
		}

		// Display and log Latitude
		oStrStream.str("");
		oStrStream << m_tracker->currentCoords->getLatitude(); // using stream to convert number to string
		logMessage = "GPS Coordinates: " + oStrStream.str();

		// Display and log Longitude
		oStrStream.str("");
		oStrStream << m_tracker->currentCoords->getLongitude(); // using stream to convert number to string
		logMessage += ", " + oStrStream.str();
		std::cout << logMessage << std::endl;
		dataLogger.log(logMessage);

		// Check location boundary
		logMessage = trackerActuator.checkBoundaryStatus();
		if (logMessage != "")
		{
			std::cout << logMessage << std::endl;
			dataLogger.log(logMessage);
		}

		// Modify battery level every 1 minute
		if ((timePassed % 60) == 0)
			m_tracker->setBatteryLevel(m_tracker->getBatteryLevel() - 1);

		// Display and log battery level
		oStrStream.str("");
		oStrStream << m_tracker->getBatteryLevel();
		logMessage = "Battery Level: " + oStrStream.str();
		std::cout << logMessage << std::endl;
		dataLogger.log(logMessage);

		// Check if the battery is low
		if (trackerActuator.hasLowBatteryLevel())
			std::cout << "Batter level is low!\n";

		std::cout << std::endl;

		// Check if a key has been pressed
		if (_kbhit())
		{
			key = _getch();
			if (key == KEY_STOP) // if the user presses 's', then stop
				stopView = true;
		}
	}
}

void Menus::setHomeCoords()
{
	const std::string LAT_PROMPT = "\nPlease enter your home latitude: ";
	const std::string LON_PROMPT = "\nPlease enter your home longitude: ";
	double latitude = 0.0;
	double longitude = 0.0;
	std::unique_ptr<Permission> permission(new Permission());

	// Check if the subject has modify permission
	if (permission->checkPermission(m_subject->getRoleName(), AccessTypes::Modify))
	{
		// Prompt for Latitude
		std::cout << LAT_PROMPT;
		latitude = UserInput::getDouble(LAT_PROMPT);

		// Check if valid and set
		while (!m_tracker->homeCoords->setLatitude(latitude))
		{
			std::cout << "\nValue must be in the range of -90 to 90.";
			std::cout << LAT_PROMPT;
			latitude = UserInput::getDouble(LAT_PROMPT);
		}

		// Prompt for Longitude
		std::cout << LON_PROMPT;
		longitude = UserInput::getDouble(LON_PROMPT);

		// Check if valid and set
		while (!m_tracker->homeCoords->setLongitude(longitude))
		{
			std::cout << "\nValue must be in the range of -180 to 180.";
			std::cout << LON_PROMPT;
			longitude = UserInput::getDouble(LON_PROMPT);
		}
	}
	else
		std::cout << "\nYou don't have permission to do that!\n";
}

void Menus::setBoundaryRadius()
{
	uint16_t boundary = 0;

	std::unique_ptr<Permission> permission(new Permission());

	// Check if the subject has modify permission
	if (permission->checkPermission(m_subject->getRoleName(), AccessTypes::Modify))
	{
		// Prompt for the boundary
		std::cout << "\nPlease enter a boundary radius("
			<< Tracker::BOUNDARY_MIN << "-" << Tracker::BOUNDARY_MAX << "): ";
		boundary = UserInput::getUInt16();

		// Check if input is within range and set
		while (!m_tracker->setBoundaryRadius(boundary))
		{
			std::cout << "\nValue is out of range!\n";
			std::cout << "\nPlease enter a boundary radius("
				<< Tracker::BOUNDARY_MIN << "-" << Tracker::BOUNDARY_MAX << "): ";
			boundary = UserInput::getUInt16();
		}
	}
	else
		std::cout << "\nYou don't have permission to do that!\n";
}