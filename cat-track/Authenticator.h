#pragma once
#include <memory>
#include <string>

#include "AuthenticationInfo.h"
#include "ProofOfId.h"
#include "Subject.h"

class Authenticator
{
public:
	std::unique_ptr<ProofOfId> authenticateUser(std::shared_ptr<Subject>);

private:
	std::string encryptDecrypt(std::string);
};