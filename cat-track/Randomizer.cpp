#include "Randomizer.h"

double Randomizer::getDouble(double min, double max)
{
	double result = 0;

	std::default_random_engine dre(m_seed());
	std::uniform_real_distribution<double> urd(min, max);

	result = urd(dre);

	return result;
}

