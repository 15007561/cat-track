#pragma once
#include <cinttypes>
#include <memory>

#include "Coordinates.h"
#include "Randomizer.h"

class Tracker;

class GpsSensor
{
private:
	static const double COORDS_VARIATION;

	enum DebugKey
	{
		None = 0,
		InBounds = 105,		// 'i' Set GPS coordinates within boundary
		OutBounds = 111		// 'o' Set GPS coordinates outside boundary
	};

public:
	// Description: Get random coordinates based on debug key or previous coordinates.
	// Parameter(s): std::shared_ptr<Tracker>, integer: Debug key
	// Returns: unique_ptr<Coordinates>
	std::unique_ptr<Coordinates> getCoordinates(const std::shared_ptr<Tracker>, int);

	// Description: Get random coordinates based on the previous coordinates.
	// Parameter(s): Tracker
	// Returns: unique_ptr<Coordinates>
	std::unique_ptr<Coordinates> getCoordinates(const std::shared_ptr<Tracker>);

	// Description: Get current coordinates. Randomly generated.
	// Parameter(s): None
	// Returns: unique_ptr<Coordinates>
	std::unique_ptr<Coordinates> getCurrentCoords();

private:
	// Description: Convert meters to decimal degrees.
	// Parameter(s): uint16_t representing meters.
	// Returns: double, representing decimal degrees. 
	//	        0, if unable to convert.
	double metersToDegrees(uint16_t);

	// Description: Get random coordinates within set radius boundary.
	// Parameter(s): Tracker
	// Returns: unique_ptr<Coordinates>
	std::unique_ptr<Coordinates> randomInBounds(const std::shared_ptr<Tracker>);
};
