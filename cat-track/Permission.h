#pragma once
#include <map>
#include "AccessTypes.h"
#include "Roles.h"

class Permission
{
private:
	std::map<unsigned, unsigned> m_roleResourceAccessMap;

public:
	// Description: Sets the roles' permissions.
	// Parameter(s): none
	Permission();

	// Description: Check the role's permission for the tracker
	// Parameter(s): Role and access type.
	// Returns: bool. True if role has permission, false if not.
	bool checkPermission(unsigned, unsigned);
};
