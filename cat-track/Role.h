#pragma once
#include <map>
#include <string>
#include "Roles.h"

class Role
{
private:
	unsigned m_roleName;
	std::map<std::string, unsigned> m_userRoleAccessMap;

public:
	Role();
	unsigned getRoleName() { return m_roleName; }
	void setRoleName(std::string);
};
