#include "Authenticator.h" 

std::unique_ptr<ProofOfId> Authenticator::authenticateUser(std::shared_ptr<Subject> subject)
{
	AuthenticationInfo authInfo = AuthenticationInfo();
	std::unique_ptr<ProofOfId> proofOfId(new ProofOfId());
	
	std::string username = subject->getUsername();
	std::string password = subject->getPassword();
	std::string token = "";
	
	if (authInfo.validateUser(username, password))
	{
		std::cout << "\nWelcome " << username << "!";
		token = encryptDecrypt(username + password);			
		subject->setProofId(token);
		proofOfId->setProofId(token);
	}
	else
		std::cout << "\nInvalid username or password! Please try again.";
	
	return proofOfId;
}

std::string Authenticator::encryptDecrypt(std::string toEncrypt)
{
	char key = 'z';
	std::string ouput = toEncrypt;

	for (unsigned i = 0; i < toEncrypt.size(); i++)
		ouput[i] = toEncrypt[i] ^ key;

	return ouput;
}

