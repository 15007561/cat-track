#pragma once
#include <string>

class Logger
{
private:
	std::string m_logFile;
	static const std::string DEFAULT_LOGFILE;
public:
	// Default constructor
	Logger();

	// Description: Constructor with string parameter.
	// Parameter(s): String with log filename.
	// Returns: nothing
	Logger(std::string);

	// Destructor
	~Logger();

	// Description: Write a message to a log file.
	// Parameter(s): Message to write.
	// Returns: nothing
	void log(std::string);
};
