#include "GpsSensor.h"
#include "Tracker.h"

const double GpsSensor::COORDS_VARIATION = 0.000009;

std::unique_ptr<Coordinates> GpsSensor::getCoordinates(const std::shared_ptr<Tracker> tracker, int key)
{
	std::unique_ptr<Coordinates> newCoords(new Coordinates());
	double boundary = metersToDegrees(tracker->getBoundaryRadius());

	if (key == DebugKey::InBounds)		// Set coordinates within boundary
	{
		newCoords = randomInBounds(tracker);
	}
	else if (key == DebugKey::OutBounds)// Set coordinates outside boundary
	{
		newCoords->setLatitude(tracker->homeCoords->getLatitude() + boundary + COORDS_VARIATION);
		newCoords->setLongitude(tracker->homeCoords->getLongitude() + boundary + COORDS_VARIATION);
	}
	else								// Set coordinates based on previous coordinates
	{
		newCoords = getCoordinates(tracker);
	}

	return newCoords;
}

std::unique_ptr<Coordinates> GpsSensor::getCurrentCoords()
{
	std::unique_ptr<Coordinates> currCoords(new Coordinates());
	Randomizer rnd;

	double lat = rnd.getDouble(Coordinates::LONGITUDE_MIN, Coordinates::LONGITUDE_MAX);
	double lon = rnd.getDouble(Coordinates::LATITUDE_MIN, Coordinates::LATITUDE_MAX);

	currCoords->setLatitude(lat);
	currCoords->setLongitude(lon);

	return currCoords;
}

double GpsSensor::metersToDegrees(uint16_t meters)
{
	const double METERS_DEGREES_RATIO = 0.000009; // 1m ~= 0.000009 decimal degrees

	if (meters > 0)
		return meters * METERS_DEGREES_RATIO;
	else
		return 0;
}

std::unique_ptr<Coordinates> GpsSensor::randomInBounds(const std::shared_ptr<Tracker> tracker)
{
	const double ANGLE = 45;
	
	Randomizer rnd;
	double boundary = 0.0;
	double latMin = 0.0;
	double latMax = 0.0;
	double lonMin = 0.0;
	double lonMax = 0.0;
	std::unique_ptr<Coordinates> newCoords(new Coordinates());
	
	// Convert boundary to rough estimate of decimal degrees
	boundary = metersToDegrees(tracker->getBoundaryRadius());

	// Calculate latitude min/max range
	latMin = tracker->homeCoords->getLatitude() - (boundary * sin(ANGLE));
	latMax = tracker->homeCoords->getLatitude() + (boundary * sin(ANGLE));

	// Calculate longitude min/max range
	lonMin = tracker->homeCoords->getLongitude() - (boundary * sin(ANGLE));
	lonMax = tracker->homeCoords->getLongitude() + (boundary * sin(ANGLE));

	// Randomize coordinates within ranges
	newCoords->setLatitude(rnd.getDouble(latMin, latMax));
	newCoords->setLongitude(rnd.getDouble(lonMin, lonMax));

	return newCoords;
}

std::unique_ptr<Coordinates> GpsSensor::getCoordinates(const std::shared_ptr<Tracker> tracker)
{
	Randomizer rnd;
	double latMin = 0.0;
	double latMax = 0.0;
	double lonMin = 0.0;
	double lonMax = 0.0;
	std::unique_ptr<Coordinates> newCoords(new Coordinates());

	// Calculate latitude min/max range
	latMin = tracker->currentCoords->getLatitude() - COORDS_VARIATION;
	latMax = tracker->currentCoords->getLatitude() + COORDS_VARIATION;

	// Calculate longitude min/max range
	lonMin = tracker->currentCoords->getLongitude() - COORDS_VARIATION;
	lonMax = tracker->currentCoords->getLongitude() + COORDS_VARIATION;

	// Randomize coordinates within ranges
	newCoords->setLatitude(rnd.getDouble(latMin, latMax));
	newCoords->setLongitude(rnd.getDouble(lonMin, lonMax));

	return newCoords;
}