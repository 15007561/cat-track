#pragma once
enum AccessTypes
{
	None = 0x00,	// No access
	View = 0x01,	// View resource
	Modify = 0x02,	// Modify resource
	Manage = 0x04,	// Manage users
};