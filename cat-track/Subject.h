#pragma once
#include <iostream>
#include <memory>
#include <string>

#include "AccessTypes.h"
#include "Role.h"
#include "Tracker.h"
#include "TrackerFactory.h"
#include "UserInput.h"

class Subject {
private:
	std::string m_username;
	std::string m_password;
	std::string m_proofId;
	Role m_role;
	std::shared_ptr<Tracker> m_tracker;

public:
	// Description: Constructor-prompts the user for credentials.
	// Parameter(s): none
	Subject();

	// Destructor
	~Subject();

	// Description: Get the user's name.
	// Parameter(s): none
	// Returns: string
	std::string getUsername() { return m_username; }

	// Description: Get the user's password.
	// Parameter(s): none
	// Returns: string
	std::string getPassword() { return m_password; }
	
	// Description: Get the user's proof of ID.
	// Parameter(s): none
	// Returns: string
	std::string getProofId() { return m_proofId; }

	// Description: Set the user's proof of ID.
	// Parameter(s): string
	// Returns: nothing
	void setProofId(std::string);

	// Description: Gets the tracker device.
	// Parameter(s): none
	// Returns: shared_ptr<Tracker>
	std::shared_ptr<Tracker> getTracker();

	// Description: Gets the subject's role name.
	// Parameter(s): none
	// Returns: unsigned integer representing role name
	unsigned getRoleName() { return m_role.getRoleName(); }
};